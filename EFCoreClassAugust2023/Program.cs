﻿using EFCoreClassAugust2023.Models;
using EFCoreClassAugust2023.Services.Professors;
using Microsoft.EntityFrameworkCore;

namespace EFCoreClassAugust2023
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            // Scaffold-DbContext "Data Source = DESKTOP-HH0G8JM\SQLEXPRESS; Initial Catalog = Chinook; Integrated Security = True; Trust Server Certificate = True;" Microsoft.EntityFrameworkCore.SqlServer -o ChinookModel
            IProfessorService professorService = new ProfessorService(new PostgradDbContext());
            professorService.AddStudent(1, 1);
        }
    }
}