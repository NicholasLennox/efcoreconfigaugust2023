﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreClassAugust2023.Models
{
    [Table(nameof(Project))]
    internal class Project
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Title { get; set; } = null!;
        public int StudentId { get; set; }
        // Navigation
        public Student Student { get; set; } // 1-1
    }
}
