﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreClassAugust2023.Models
{
    [Table(nameof(Professor))]
    internal class Professor
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; } = null!;
        public string? Field { get; set; }
        // Navigation
        public ICollection<Student> Students { get; set; } // 1-M
        public ICollection<Subject> Subjects { get; set; } // 1-M
    }
}
