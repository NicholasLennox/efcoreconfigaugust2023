﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreClassAugust2023.Models
{
    [Table(nameof(Subject))]
    internal class Subject
    {
        public int Id { get; set; }
        [StringLength(10)]
        public string Code { get; set; } = null!;
        [StringLength(100)]
        public string Title { get; set; } = null!;
        public int? ProfessorId { get; set; }
        // Navigation 
        public ICollection<Student> Students { get; set; } // M-M
        public Professor Professor { get; set; } // M-1
    }
}
