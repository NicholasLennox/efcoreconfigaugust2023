﻿using EFCoreClassAugust2023.Exceptions;
using EFCoreClassAugust2023.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreClassAugust2023.Services.Professors
{
    internal class ProfessorService : IProfessorService
    {
        // Dependency Injection
        private readonly PostgradDbContext _context;

        public ProfessorService(PostgradDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Registers a new student for a professor to supervise.
        /// Professors can supervise a maximum of 5 students at a time.
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="professorId"></param>
        /// <exception cref="ProfessorException"></exception>
        /// <exception cref="StudentException"></exception>
        public void AddStudent(int studentId, int professorId)
        {
            // Early exists/gaurds
            if (!_context.Professors.Any(p => p.Id == professorId))
                throw new ProfessorException("Professor does not exist with ID");
            if (!_context.Students.Any(s => s.Id == studentId))
                throw new StudentException("Student does not exist with ID");

            // Proceed with "good path"
            var professor = _context.Professors
                .Where(p => p.Id == professorId)
                .Include(p => p.Students)
                .First();

            // Business logic gaurd
            if(professor.Students.Count >= 5)       
                throw new ProfessorException("Professor has too many students");

            // All good, we can update
            var student = _context.Students.First(s => s.Id == studentId);
            professor.Students.Add(student);
            _context.SaveChanges();
        }

        /// <summary>
        /// Registers a new subject for a professor to lecture.
        /// Professors can lecture a maximum of 3 subjects at a time.
        /// </summary>
        /// <param name="subjectId"></param>
        /// <param name="professorId"></param>
        /// <exception cref="ProfessorException"></exception>
        /// <exception cref="SubjectException"></exception>
        public void AddSubject(int subjectId, int professorId)
        {
            // Early exists/gaurds
            if (!_context.Professors.Any(p => p.Id == professorId))
                throw new ProfessorException("Professor does not exist with ID");
            if (!_context.Subjects.Any(s => s.Id == subjectId))
                throw new SubjectException("Subject does not exist with ID");

            // Proceed with "good path"
            var professor = _context.Professors
                .Where(p => p.Id == professorId)
                .Include(p => p.Subjects)
                .First();

            // Business logic gaurd
            if (professor.Students.Count >= 3)
                throw new ProfessorException("Professor has too many subjects");

            // All good, we can update
            var subject = _context.Subjects.First(s => s.Id == subjectId);
            professor.Subjects.Add(subject);
            _context.SaveChanges();
        }

        /// <summary>
        /// Deletes a Professor. The relationships with subjects and students are severed (set null).
        /// </summary>
        /// <param name="id"></param>
        /// <exception cref="ProfessorException"></exception>
        public void Delete(int id)
        {  
            // Early exit if prof doesnt exist
            if(!_context.Professors.Any(p => p.Id == id))
                throw new ProfessorException("Professor does not exist with ID");

            var prof = _context.Professors
                .Where(p => p.Id == id)
                .First();

            // Want to remove related entities to not cause referential problems
            // https://learn.microsoft.com/en-us/ef/core/saving/cascade-delete#severing-a-relationship
            // Only works if relationship is nullable
            prof.Students.Clear();
            prof.Subjects.Clear();
            // Can safely remove professor
            _context.Professors.Remove(prof);
            _context.SaveChanges();
        }

        /// <summary>
        /// Get all professors from the database.
        /// Does not include related entities.
        /// </summary>
        /// <returns></returns>
        public ICollection<Professor> GetAll()
        {
            return _context.Professors.ToHashSet();
        }

        /// <summary>
        /// Gets a professor by their ID.
        /// Throws an exception if the professor does not exist in the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="ProfessorException"></exception>
        public Professor GetById(int id)
        {
            var prof = _context.Professors.Where(p => p.Id == id).FirstOrDefault();

            return prof is null ? throw new ProfessorException("Professor does not exist with that ID") : prof;
        }

        /// <summary>
        /// Gets the subjects for a professor. 
        /// Throws an exception if the professor does not exist.
        /// If there are no subjects, no exception is thrown, an empty set is returned.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="ProfessorException"></exception>
        public ICollection<Subject> GetSubjects(int id)
        {
            if (!_context.Professors.Any(p => p.Id == id))
                throw new ProfessorException("Professor does not exist with the ID");

            // Its a smaller SQL statement if we go from subjects (no joining)
            return _context.Subjects
                .Where(s => s.ProfessorId == id)
                .ToHashSet();
        }

        /// <summary>
        /// Saves a new professor to the database.
        /// If you add related entities (students, subjects) the business logic rules wont apply.
        /// This is something you can change by forcing .Clear() on the professor.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Professor Save(Professor entity)
        {
            _context.Professors.Add(entity);
            _context.SaveChanges();
            return entity; // entity is updated with the new ID (reference type)
        }

        /// <summary>
        /// If you add related entities (students, subjects) the business logic rules wont apply.
        /// This is something you can change by forcing .Clear() on the professor.
        /// You can create separate methods to update the related entities.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Professor Update(Professor entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
            return entity;
        }
    }
}
