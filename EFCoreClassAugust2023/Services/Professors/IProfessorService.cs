﻿using EFCoreClassAugust2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreClassAugust2023.Services.Professors
{
    internal interface IProfessorService : ICrudService<Professor, int>
    {
        void AddStudent(int StudentId, int professorId);
        ICollection<Subject> GetSubjects(int id);
        void AddSubject(int subjectId, int professorId);
    }
}
