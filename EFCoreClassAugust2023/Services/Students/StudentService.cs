﻿using EFCoreClassAugust2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreClassAugust2023.Services.Students
{
    internal class StudentService : IStudentService
    {
        public void Delete(int key)
        {
            throw new NotImplementedException();
        }

        public ICollection<Student> GetAll()
        {
            throw new NotImplementedException();
        }

        public Student GetById(int key)
        {
            throw new NotImplementedException();
        }

        public Student GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public Student Save(Student entity)
        {
            throw new NotImplementedException();
        }

        public Student Update(Student entity)
        {
            throw new NotImplementedException();
        }
    }
}
