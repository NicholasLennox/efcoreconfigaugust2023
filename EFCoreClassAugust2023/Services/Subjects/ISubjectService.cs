﻿using EFCoreClassAugust2023.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreClassAugust2023.Services.Subjects
{
    internal interface ISubjectService : ICrudService<Subject, int>
    {
    }
}
