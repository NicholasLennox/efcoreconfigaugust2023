﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCoreClassAugust2023.Exceptions
{
    internal class ProjectException : Exception
    {
        public ProjectException(string? message) : base(message)
        {
        }
    }
}
