﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EFCoreClassAugust2023.Migrations
{
    /// <inheritdoc />
    public partial class addingSeeding : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Dob", "Email", "Name", "ProfessorId" },
                values: new object[,]
                {
                    { 1, new DateTime(2023, 9, 18, 14, 15, 59, 222, DateTimeKind.Local).AddTicks(8519), null, "Bobby", null },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Alice", null }
                });

            migrationBuilder.InsertData(
                table: "Subject",
                columns: new[] { "Id", "Code", "ProfessorId", "Title" },
                values: new object[] { 1, "ABCDE", null, "Learning to cry in the shower" });

            migrationBuilder.InsertData(
                table: "StudentSubject",
                columns: new[] { "StudentsId", "SubjectsId" },
                values: new object[] { 1, 1 });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "StudentSubject",
                keyColumns: new[] { "StudentsId", "SubjectsId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "Student",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Subject",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
